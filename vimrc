let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim' " find config
if empty(glob(data_dir . '/autoload/plug.vim')) " install VimPlug if not installed
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)')) " Run PlugInstall if there are missing plugins
  \| PlugInstall --sync | source $MYVIMRC
\| endif

call plug#begin()

Plug 'itchyny/lightline.vim'
Plug 'godlygeek/tabular'
Plug 'preservim/vim-markdown'
Plug 'preservim/nerdtree'

call plug#end()

set tabstop=4
set shiftwidth=4
set expandtab

set number relativenumber

" Colorscheme
syntax enable
colorscheme jellybeans
set laststatus=2
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ }

" Folds
set nofoldenable

" Keymaps
nnoremap <SPACE> <Nop>
let mapleader=" "

" quick write
noremap <Leader>w :update<CR>
noremap <Leader>q :wq<CR>
" quick timestamp
nnoremap <F5> "=strftime("%Y-%m-%d %H:%M")<CR>P
inoremap <F5> <C-R>=strftime("%Y-%m-%d %H:%M")<CR>
" NERDTree
nnoremap <Leader>e :NERDTreeToggle<CR>


" vim-markdown
let g:vim_markdown_math = 1
let g:vim_markdown_strikethrough = 1
let g:vim_markdown_follow_anchor = 1
